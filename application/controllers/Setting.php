<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller 
{
public $data = [];

    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library(['ion_auth', 'form_validation']);
        $this->load->helper(['url', 'language']);

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
         $this->load->helper('option_helper');
        $this->lang->load('auth');
        $this->load->model('Setting_Model');
    }


    public function index()
    {

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth/login', 'refresh');
        }elseif (!$this->ion_auth->is_admin()){ // remove this elseif if you want to enable this for non-admins
        
            // redirect them to the home page because they must be an administrator to view this
            show_error('You must be an administrator to view this page.');
        }else{
            $this->data['title'] = $this->lang->line('index_heading');
            
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            //list the users
            $this->data['users'] = $this->ion_auth->users()->result();
            
            //USAGE NOTE - you can do more complicated queries like this
            //$this->data['users'] = $this->ion_auth->where('field', 'value')->users()->result();
            
            foreach ($this->data['users'] as $k => $user){
                $this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
            }
            $data["title"] = "Logo Settings";
            $data['content'] = $this->load->view('setting/general', $this->data ,true);
            $this->load->view('layout/main',$data);
            // $this->_render_page('auth' . DIRECTORY_SEPARATOR . 'index', $this->data);
        }
    }

      public function textupload(){
            $data = array(
                "text" => $this->input->post('text'),
                 "status" =>$this->input->post('status')


            );
            $this->Setting_Model->updatelogo($data);
        }

  
  
    public function imageUpload() {
        // print_R($_FILES);exit;
        if (!empty($_FILES['file']['name'])) {
           $fileInfo = pathinfo($_FILES['file']['name']);
           //if($fileInfo['extension'] == 'jpg' OR )
           $newName = time() . '.' . $fileInfo['extension'];
           $move = move_uploaded_file($_FILES['file']['tmp_name'], "assets/myUploads/" . $newName);
           // echo $_FILES['name'];
           // die();
       }           
        
       // die();
       $data = array(

         "logo"  => ($newName),
          "status"=> $this->input->post('status')

       );     

            $this->Setting_Model->updatelogo($data);

            

                if ( empty($move))
                {
                        // $error = array('error' => $this->upload->display_errors());
                        $status = "error";
                        $this->session->set_flashdata('status', $status);
                        $data['title'] ="Profile";  
                
                        $data['content'] = $this->load->view('setting/general',$data,true);
                        $this->load->view('layout/main',$data);

                        
                }
                else
                {
                        $status = "success";
                        $this->session->set_flashdata('status', $status);
                        $data = array('setting' => $this->upload->data());

                        $data['title'] ="Profile";  
                
                        $data['content'] = $this->load->view('setting/general',$data,true);
                        $this->load->view('layout/main',$data);
                }
        
    }


  

      
}
