  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url() ?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo ucWords($this->ion_auth->user()->row()->first_name ." ".$this->ion_auth->user()->row()->last_name);?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
         <li><a href="#"><span>Dashboard</span></a></li>
         <li class="treeview">
                <a href="#"> 
                    <span>Users</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url('') ?>"><i class="fa fa-circle-o"></i>All Users </a></li>
                    <li><a href="<?php echo base_url('Auth/create_user') ?>"><i class="fa fa-circle-o"></i>Add Users</a></li>  
                    
                         
                </ul>
          </li>
          <li class="treeview">
                <a href="#"> 
                    <span>Groups</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url('Auth/view_groups') ?>"><i class="fa fa-circle-o"></i> All Groups</a></li> 
                    <li><a href="<?php echo base_url('Auth/create_group') ?>"><i class="fa fa-circle-o"></i>Add Group</a></li>                   
                </ul>
          </li>
          <li class="treeview">
                <a href="#"> 
                    <span>Tools</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url('tools/') ?>"><i class="fa fa-circle-o"></i>Login Attempts</a></li>       
                </ul>
          </li>
          <li class="treeview">
                <a href="#"> 
                    <span>Setting</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url('setting/') ?>"><i class="fa fa-circle-o"></i>General</a></li>  
                    <li><a href="<?php echo base_url('auth/edit_user/'.$this->ion_auth->user()->row()->id);?>"><i class="fa fa-circle-o"></i> Profile</a></li>
                    <li><a href="<?php echo base_url('') ?>"><i class="fa fa-circle-o"></i>DataBase Backup</a></li>      
                </ul>
          </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>