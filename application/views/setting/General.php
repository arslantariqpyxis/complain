     
  
    <!-- Content Header (Page header) -->
 

    <!-- Main content -->
    <section class="content">
<?php if(!empty($this->session->flashdata('status'))){echo $this->session->flashdata('status');}?>
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Logo</h3>

        </div>
        <div class="box-body">
          <div class="form-group ">
            
              <label>Select Option</label>
                  <select class="form-control " id="attendance">
                    <option value="0">---Select Option---</option>
                    <option value="image">Image</option>
                    <option value="text">Text</option>
                  </select>
                </div>

                <div id="image">
                    <form action="<?php echo base_url('Setting/imageUpload') ?>" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                  <div class="file btn btn-primary">
              Upload
              <input type="file" id="imgInp" name="file" value="" class="btn btn-primary btn-file"/>
            </div>
 
                 <img src="" id='img-upload'class="form-group col-sm-2" style="width: 155px;"/>
                 <div class="form-group">
                  <input type="hidden" name="status" value="image" >
                 <input type="text" name="logo" class="form-control" id="img-text" value="" style="background: white;" readonly>
               </div>
                 <div class="form-group">
                 <button type="submit" class=" btn btn-primary" style="margin-top: -3px;">Save Changes</button>
               </div>
               </form>
                </div>
                <div id="text">
                  <form method="post" action="<?php echo base_url('Setting/textUpload');   ?>">
                  <div class="form-group">
                  <input type="hidden" name="status" value="text">
                  <input type="text" name="text" class="form-control"> 
                  <div class="form-group">
                 <button type="submit" class=" btn btn-primary" style="margin-top: 15px;">Save Changes</button>
               </div>
                </div> 
              </form>
                </div>
        </div>
        <!-- /.box-body -->

      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->


  <script type="text/javascript">
    $("#attendance").change(function() {
  var id = $(this).val();
if(id == "image"){
$("#image").show();
$("#text").hide();
}else if(id == "text"){
  $("#text").show();
  $("#image").hide();
}
}).change();
  </script>
<script type="text/javascript">
  $(document).ready(function(){
function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
              
                $('#img-upload').attr('src', e.target.result);
                 $('#img-text').attr('value', input.files[0].name);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imgInp").change(function(){
        readURL(this);
    });
       });
</script>

 <style type="text/css">
    #image,#text{
display:none;
}
div.file {
  position: relative;
  overflow: hidden;
  margin-bottom: 10px;
}
.btn-file {
  position: absolute;
  font-size: 0px;
      margin-top: -18px;
  opacity: 0;
  right: 0;
  top: 0;
}
  </style>