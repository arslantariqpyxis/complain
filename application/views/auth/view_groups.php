<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Admin Details</h3>
    </div>
    <div class="box-body">

        <table cellpadding="0" cellspacing="10" class="table table-responsive table-striped table-bordered table-hover" id="example1">
              <thead>
                <tr>
                    <th>Sr.#</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Action</th>
                  </tr>
              </thead>
                <tbody>
                  
                  <?php $i=0;foreach ($result as $row): $i++;?>
                    <tr>
                      <td><?php echo $i;?></td>
                      <td> <?php echo  $row->name ?></td>
                      <td> <?php echo  $row->description ?></td>
                      <td><a href="<?php echo base_url()?>auth/edit_group/<?php echo $row->id?>"><i class="fa fa-edit" style="font-size:24px"></i></a></td>
      
                    </tr>
                  <?php endforeach;?>
                </tbody>
        </table>
    </div> 
  </div>
</section>

 