<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<style type="text/css">
  body {
  margin: 0;
  padding: 0;
  background-color: #17a2b8;
  height: 100vh;
}
#login .container #login-row #login-column #login-box {
    margin-top: 120px;
    max-width: 600px;
    height: 320px;
    border: 1px solid #ffffff;
    border-radius: 10px;
    background-color: #eaeaeacc;
}
#login .container #login-row #login-column #login-box #login-form {
  padding: 20px;
}
#login .container #login-row #login-column #login-box #login-form #register-link {
  margin-top: -85px;
}
.fade-in {
  opacity: 1;
  animation-name: fadeInOpacity;
  animation-iteration-count: 1;
  animation-timing-function: ease-in;
  animation-duration: 1s;
}

@keyframes fadeInOpacity {
  0% {
    opacity: 0;
  }
  100% {
    opacity: 1;
  }
}
</style>

<body>
    <div id="login">
      <h3 class="text-center text-white pt-5"> <div id="infoMessage"><?php echo $message;?></div></h3>

        <div class="container fade-in">
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-6">
                    <div id="login-box" class="col-md-12">
                        <form id="login-form" class="form" action="" method="post">
                            <h3 class="text-center text-info">Login</h3>
                            
                            <?php echo form_open("auth/login");?>
                            <div class="form-group">
                                <label for="username" class="text-info">Username:</label><br>
                                <input type="text" name="identity" value="" id="identity" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="password" class="text-info">Password:</label><br>
                                <input type="password" name="password" value="" id="password" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="remember-me" class="text-info"><span>Remember me</span> <span><input type="checkbox" name="remember" value="1" id="remember">
                                </span></label><br>
                                <input type="submit" name="submit" class="btn btn-info btn-md" value="Login">
                            </div>
                            <div id="register-link" class="text-right">
                                <!-- <a href="#" class="text-info">Register here</a> -->
                                <a href="forgot_password" class="text-info">Forgot your password?</a>
                            </div>
                            <?php echo form_close();?>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

<!-- <h1><?php //echo lang('login_heading');?></h1>
<p><?php //echo lang('login_subheading');?></p>

<div id="infoMessage"><?php echo $message;?></div>

<?php //echo form_open("auth/login");?>

  <p>
    <?php //echo lang('login_identity_label', 'identity');?>
    <?php //echo form_input($identity);?>
  </p>

  <p>
    <?php //echo lang('login_password_label', 'password');?>
    <?php //echo form_input($password);?>
  </p>

  <p>
    <?php //echo lang('login_remember_label', 'remember');?>
    <?php //echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
  </p>


  <p><?php //echo form_submit('submit', lang('login_submit_btn'));?></p>

<?php //echo form_close();?>

<p><a href="forgot_password"><?php //echo lang('login_forgot_password');?></a></p> -->