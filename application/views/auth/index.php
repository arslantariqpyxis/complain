<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Admin Details</h3>
    </div>
    <div class="box-body">
      <div id="infoMessage"><?php echo $message;?></div>
        <table cellpadding="0" cellspacing="10" class="table table-responsive table-striped table-bordered table-hover" id="example1">
              <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Groups</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
              </thead>
                <tbody>
                  
                  <?php foreach ($users as $user):?>
                    <tr>
                      <td> <?php echo  $user->first_name ?></td>
                      <td> <?php echo  $user->last_name ?></td>
                      <td> <?php echo  $user->email ?></td>
                      <td>
                      <?php foreach ($user->groups as $group):?>
                      <a href="<?php echo base_url('auth/edit_group/').$group->id ?>"><?php echo $group->name ?></a><br>
                      <?php endforeach?>
                      </td>
                      <td><a href="<?php echo base_url('auth/deactivate/').$user->id ?>">Active</a></td>
                      <td><a href="<?php echo base_url('auth/edit_user/').$user->id ?>"><i class="fa fa-edit" style="font-size:24px"></i></a></td>
                    </tr>
                  <?php endforeach;?>
                </tbody>
        </table>

       <p><a href="<?php echo base_url('auth/create_user') ?>">Create a new user</a> | <a href="<?php echo base_url('auth/create_group') ?>">Create a new group</a></p>     
    </div> 
  </div>
</section>

 <!--  <h1><?php echo lang('index_heading');?></h1>
              <p><?php echo lang('index_subheading');?></p>
          <div id="infoMessage"><?php echo $message;?></div>
          <table cellpadding=0 cellspacing=10>
            <tr>
              <th><?php echo lang('index_fname_th');?></th>
              <th><?php echo lang('index_lname_th');?></th>
              <th><?php echo lang('index_email_th');?></th>
              <th><?php echo lang('index_groups_th');?></th>
              <th><?php echo lang('index_status_th');?></th>
              <th><?php echo lang('index_action_th');?></th>
            </tr>
            <?php foreach ($users as $user):?>
              <tr>
                      <td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
                      <td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
                      <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
                <td>
                  <?php foreach ($user->groups as $group):?>
                    <?php echo anchor("auth/edit_group/".$group->id, htmlspecialchars($group->name,ENT_QUOTES,'UTF-8')) ;?><br />
                          <?php endforeach?>
                </td>
                <td><?php echo ($user->active) ? anchor("auth/deactivate/".$user->id, lang('index_active_link')) : anchor("auth/activate/". $user->id, lang('index_inactive_link'));?></td>
                <td><?php echo anchor("auth/edit_user/".$user->id, 'Edit') ;?></td>
              </tr>
            <?php endforeach;?>
          </table>

          <p><?php echo anchor('auth/create_user', lang('index_create_user_link'))?> | <?php echo anchor('auth/create_group', lang('index_create_group_link'))?></p>
 -->

         
         
              
     
