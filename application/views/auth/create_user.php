 <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Title</h3>
        </div>
        <div class="box-body">
          <div id="infoMessage"><?php echo $message;?></div>
          <form action="<?php echo base_url('auth/create_user') ?>" method="post" accept-charset="utf-8">
            <div class="form-group">
              <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                  <label for="first_name">First Name:</label> <br>
                  <input type="text" name="first_name" class="form-control" value="" id="first_name">
                </div>
              </div>
              <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                  <label for="last_name">Last Name:</label> <br>
                   <input type="text" name="last_name" class="form-control" value="" id="last_name">
                </div>
              </div>
              <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                  <label for="company">Company Name:</label> <br>
                  <input type="text" name="company" class="form-control" value="" id="company">
                </div>
              </div>
              <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                  <label for="phone">Phone:</label> <br>
                  <input type="text" name="phone" class="form-control" value="" id="phone">
                </div>
              </div>
              <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                  <label for="email">Email:</label> <br>
                  <input type="text" name="email" class="form-control" value="" id="email">
                </div>
              </div>
              <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                  <label for="password">Password:</label> <br>
                  <input type="password" name="password" class="form-control" value="" id="password">
                </div>
              </div>
              <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                   <label for="password_confirm">Confirm Password:</label> <br>
                  <input type="password" name="password_confirm" class="form-control" value="" id="password_confirm">
                </div>
              </div> 
              <div >
              <input type="submit" style="margin-left: 47%;margin-top: 1%;color: white;background-color:#17a2b8 " name="submit" class="btn btn-info" value="Submit">  
              </div>             
            </div>
          </form>
        </div>
        <!-- /.box-body -->
        
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>






<!-- <h1><?php echo lang('create_user_heading');?></h1>
<p><?php echo lang('create_user_subheading');?></p>

<div id="infoMessage"><?php echo $message;?></div>

<?php echo form_open("auth/create_user");?>

      <p>
            <?php echo lang('create_user_fname_label', 'first_name');?> <br />
            <?php echo form_input($first_name);?>
      </p>

      <p>
            <?php echo lang('create_user_lname_label', 'last_name');?> <br />
            <?php echo form_input($last_name);?>
      </p>
      
      <?php
      if($identity_column!=='email') {
          echo '<p>';
          echo lang('create_user_identity_label', 'identity');
          echo '<br />';
          echo form_error('identity');
          echo form_input($identity);
          echo '</p>';
      }
      ?>

      <p>
            <?php echo lang('create_user_company_label', 'company');?> <br />
            <?php echo form_input($company);?>
      </p>

      <p>
            <?php echo lang('create_user_email_label', 'email');?> <br />
            <?php echo form_input($email);?>
      </p>

      <p>
            <?php echo lang('create_user_phone_label', 'phone');?> <br />
            <?php echo form_input($phone);?>
      </p>

      <p>
            <?php echo lang('create_user_password_label', 'password');?> <br />
            <?php echo form_input($password);?>
      </p>

      <p>
            <?php echo lang('create_user_password_confirm_label', 'password_confirm');?> <br />
            <?php echo form_input($password_confirm);?>
      </p>


      <p><?php echo form_submit('submit', lang('create_user_submit_btn'));?></p>

<?php echo form_close();?>
  -->