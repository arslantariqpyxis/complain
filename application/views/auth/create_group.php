<section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Enter Details</h3>
        </div>
        <div class="box-body">
        	<div id="infoMessage"><?php echo $message;?></div>
          <form action="<?php echo base_url('auth/create_group')?>" method="post" accept-charset="utf-8">
            <div class="form-group">
              <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                  <label for="group_name">Group Name:</label> <br>
                  <input type="text" name="group_name" class="form-control" value="" id="group_name" class="form_group">
                </div>
              </div>
              <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                  <label for="description">Description:</label> <br>
                  <input type="text" name="description" class="form-control" value="" id="description">
                </div>
              </div>
              <div class="row">
                <input type="submit" style="margin-left: 46%;margin-top: 1%;color: white;background-color:#17a2b8 " name="submit" class="btn btn-info" value="Submit">  
              </div>
            </div>
        </form>
        </div>
        <!-- /.box-body -->

      </div>
      <!-- /.box -->

    </section>


<!-- <h1><?php echo lang('create_group_heading');?></h1>
<p><?php echo lang('create_group_subheading');?></p>

<div id="infoMessage"><?php echo $message;?></div>

<?php echo form_open("auth/create_group");?>

      <p>
            <?php echo lang('create_group_name_label', 'group_name');?> <br />
            <?php echo form_input($group_name);?>
      </p>

      <p>
            <?php echo lang('create_group_desc_label', 'description');?> <br />
            <?php echo form_input($description);?>
      </p>

      <p><?php echo form_submit('submit', lang('create_group_submit_btn'));?></p>

<?php echo form_close();?> -->