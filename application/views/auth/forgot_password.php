<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<style type="text/css">
  body {
  margin: 0;
  padding: 0;
  background-color: #17a2b8;
  height: 100vh;
}
#login .container #login-row #login-column #login-box {
    margin-top: 120px;
    max-width: 600px;
    height: 227px;
    border: 1px solid #ffffff;
    border-radius: 10px;
    background-color: #eaeaeacc;
}
#login .container #login-row #login-column #login-box #login-form {
  padding: 20px;
}
#login .container #login-row #login-column #login-box #login-form #register-link {
  margin-top: -85px;
}
</style>

<body>
    <div id="login">
      <h3 class="text-center text-white pt-5"> <div id="infoMessage"><?php echo $message;?></div></h3>

        <div class="container">
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-6">
                    <div id="login-box" class="col-md-12">
                        <form id="login-form" class="form" action="" method="post">
                            <h3 class="text-center text-info">Forgot Password</h3>
                            
                            <?php echo form_open("auth/forgot_password");?>
                            <div class="form-group">
                                <label for="username" class="text-info">Email:</label><br>

                                <input type="text" name="identity" value="" id="identity" class="form-control">
                            </div>
                            <div id="" class="text-right">
                                <!-- <a href="#" class="text-info">Register here</a> -->
                                <a href="login" class="text-info">Already Have Account?</a>
                            </div>
                                <input type="submit" name="submit" class="btn btn-info btn-md" value="Login">
                            </div>
                            
                            <?php echo form_close();?>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

<!-- 
<h1><?php echo lang('forgot_password_heading');?></h1>
<p><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></p>

<div id="infoMessage"><?php echo $message;?></div>

<?php echo form_open("auth/forgot_password");?>

      <p>
      	<label for="identity"><?php echo (($type=='email') ? sprintf(lang('forgot_password_email_label'), $identity_label) : sprintf(lang('forgot_password_identity_label'), $identity_label));?></label> <br />
      	<?php echo form_input($identity);?>
      </p>

      <p><?php echo form_submit('submit', lang('forgot_password_submit_btn'));?></p>

<?php echo form_close();?> -->
