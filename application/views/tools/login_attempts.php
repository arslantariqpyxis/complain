<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Login Details</h3>
    </div>
    <div class="box-body">
        <table cellpadding="0" cellspacing="10" class="table table-responsive table-striped table-bordered table-hover" id="example1">
              <thead>
                <tr>
                    <th>Sr.#</th>
                    <!-- <th>IP Address</th> -->
                    <th>Email/Username</th>
                    <th>Date</th>
                  </tr>
              </thead>
                <tbody>
                  
                  <?php $i= 0; foreach ($result as $row):
                    $i++;
                    ?>
                    <tr>
                      <td><?php echo $i;?></td>
                      <!-- <td> <?php echo  inet_ntop($row->ip_address) ?></td>-->
                      <td> <?php echo  $row->login ?></td>
                      <td><?php echo date("d-m-Y", $row->time);?></td>
                      
                    </tr>
                  <?php endforeach;?>
                </tbody>
        </table>
    </div> 
  </div>
</section>

     
